# Your tests here
from src import task_1, task_2, task_3


# Example usage:
keywords = ['milk', 'chocolate', 'c+', 'python', 'cat', 'dog']
result = task_1(keywords)
print(result)


text = "Additifs nutritionnels : Vitamine C-D3 : 160 UI, Fer (3b103) : 4mg, Iode (3b202) : 0,28 mg, Cuivre (3b405, 3b406) : 2,2 mg,Manganèse (3b502, 3b503, 3b504) : 1,1 mg, Zinc (3b603,3b605, 3b606) : 11 mg –Clinoptilolited’origine sédimentaire : 2 g. Protéine : 11,0 % - Teneur en matières grasses : 4,5 % - Cendres brutes : 1,7 % - Cellulose brute : 0,5 % - Humidité : 80,0 %."
result = task_2(text)
print(result)


list1 = [1, 2, [[4, 5], [4, 7]], 5, 4, [[95], [2]]]
list2 = [5, 9, 4, [[8, 7]], 4, 7, 1, [[5]]]

result = task_3(list1)
result2 = task_3(list2)

print(result)
print(result2)
