# Your code here
import re

def task_1(keywords):
    sequences_count = {}

    for keyword in keywords:
        # Iterate through each character in the keyword to find sequences
        for i in range(len(keyword)):
            for j in range(i + 2, len(keyword) + 1):
                # Extract the sequence of characters
                sequence = keyword[i:j]
                # Increment the count for this sequence in the dictionary
                sequences_count[sequence] = sequences_count.get(sequence, 0) + 1

    # Find the sequence with the maximum count
    max_count = 0
    most_recurrent_sequence = ""
    for sequence, count in sequences_count.items():
        if count > max_count:
            max_count = count
            most_recurrent_sequence = sequence

    return most_recurrent_sequence



def task_2(text):
    nutrition_dict = {}

    # Extract additives nutritional values using regex
    additives_pattern = re.compile(r'Additifs nutritionnels : (.+)')
    additives_match = additives_pattern.search(text)
    if additives_match:
        additives_text = additives_match.group(1)
        # Extract individual additives and their values
        additives_list = re.findall(r'([^:,]+) : ([^,]+),?', additives_text)
        for additive in additives_list:
            nutrition_dict[additive[0].strip()] = additive[1].strip()

    # Extract other nutritional values using regex
    other_nutrition_pattern = re.compile(r'(\w+ \(.+\)) : (\d+(\.\d+)? [a-zA-Z%]+)')
    other_nutrition_matches = other_nutrition_pattern.findall(text)
    for match in other_nutrition_matches:
        nutrition_dict[match[0]] = match[1]

    return nutrition_dict


def flat_list(arg):
    flat = []
    for i in arg:
        if isinstance(i, list):
            flat.extend(flat_list(i))
        elif isinstance(i, int):
            flat.append(i)
    return flat

def task_3(arg):
    flat = flat_list(arg)
    for j in range(len(flat) - 1):
        for k in range(j+1, len(flat)):
            product = flat[j]
            for l in range(j+1, k+1):
                product *= flat[l]
            if product in flat:
                return "It's a grandma list"
    return "Not a grandma list"
